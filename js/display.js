const LUMA_THRESHOLD = 80;

function displaySchedules(schedules, schedulesElem){
	schedulesElem.innerHTML = "";
	if(schedules.length == 0){
		schedules.innerHTML = "<h2>There are no possible schedules</h2>";
	}

	for(var s=0;s<schedules.length;s++){
		var containerElem = document.createElement("SPAN");
		containerElem.className = "schedule_thumbnail";
		schedulesElem.appendChild(containerElem);
		
		var canvasElem = document.createElement("CANVAS");
		canvasElem.width = containerElem.clientWidth;
		canvasElem.height = containerElem.clientHeight - 32;
		containerElem.appendChild(canvasElem);
		
		var courseNumsElem = document.createElement("DIV");
		courseNumsElem.className = "course_numbers";
		for (var i = 0; i < schedules[s].length; i++) {
			courseNumsElem.innerHTML += schedules[s][i].classCode + (i!=schedules[s].length-1?", ":"");
		}
		containerElem.appendChild(courseNumsElem);

		


		drawClasses(canvasElem, schedules[s]);
	}
}

function displayCourseOptions(courses, optionsElem){
	optionsElem.innerHTML = "";
	

	for (var i = 0; i < courses.length; i++) {
		


		var course = courses[i];
		var containerElem = document.createElement("LI");
		
		var checkBoxElem = document.createElement("INPUT");
		checkBoxElem.id = "is_enabled";
		checkBoxElem.type = "checkbox";
		checkBoxElem.checked = courses[i].isEnabled;
		checkBoxElem.onchange = onOptionsUpdate;
		containerElem.appendChild(checkBoxElem);
		
		var labelElem = document.createElement("LABEL");
		labelElem.id="course_title";
		labelElem.innerHTML += course.title;
		containerElem.appendChild(labelElem);
		
		var colorElem = document.createElement("INPUT");
		colorElem.id = 'course_color';
		colorElem.type = "color";
		colorElem.value = courses[i].color;
		colorElem.className = "color_picker";
		colorElem.onchange = onOptionsUpdate;
		containerElem.appendChild(colorElem);

		var removeElem = document.createElement("INPUT");
		removeElem.className= 'course_remove';
		removeElem.type = 'button';
		removeElem.value = "X";
		removeElem.courseNum = i;
		removeElem.onclick = function(){
			courses.splice(this.courseNum,1);
			display();
		}
		containerElem.appendChild(removeElem);

		var classesElem = document.createElement("UL");
		classesElem.id = "course_classes"
		classesElem.className = "class_times";
		containerElem.appendChild(classesElem);

		
		for(var c=0;c<course.classes.length;c++){
			var classContainerElem = document.createElement("LI");
			classContainerElem.className = "class_time";
			

			var classCheckBoxElem = document.createElement("INPUT");
			classCheckBoxElem.id = "is_class_enabled";
			classCheckBoxElem.type = "checkbox";
			classCheckBoxElem.checked = courses[i].classes[c].isEnabled;
			classCheckBoxElem.onchange = onOptionsUpdate;
			classContainerElem.appendChild(classCheckBoxElem);
			
			var classLabelElem = document.createElement("LABEL");
			classLabelElem.id="course_title";
			classLabelElem.innerHTML += course.classes[c].classCode;
			for (var t = 0; t < course.classes[c].times.length; t++) {
				var time = course.classes[c].times[t];
				classLabelElem.innerHTML += "\n" + DAY_OF_WEEK[time.day] + timeToString(time.hours[0]) + "-" + timeToString(time.hours[1]);
			};
			classContainerElem.appendChild(classLabelElem);
			classesElem.appendChild(classContainerElem);
	
		}
		
		containerElem = optionsElem.appendChild(containerElem);
		
		//heckBoxElem = containerElem.childNodes.item("is_enabled");
		//checkBoxElem.checked = options[i].isEnabled;
	};
}

function randomColor(){

	var luma;
	var colorNum;
	do{	
		colorNum = Math.ceil(Math.random()*0xFFFFFF);
		var r = Math.floor(colorNum/(256*256));
		var g = Math.floor((colorNum%(256*256))/256);
		var b = Math.floor(colorNum%256);
		//lumanescence formula courtesy of Alnitak from StackOverflow
		luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
	}while(luma < 80)

	
	var colorStr = colorNum.toString(16);
	while(colorNum.length<6){
		colorStr = "0"+colorStr;
	}
	return "#"+colorStr;
}