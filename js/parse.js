const CLASS_DELIMITER = "Class	Section	Days & Times	Room	Instructor	Meeting Dates	Status";
const COURSE_DELIMITER = /section [A-Z]+\ [0-9]+\ -/gm;
const COURSE_TITLE_REGEX = /[A-Z]+\ [0-9]+\ -\ [A-z\ ]+/gm;
const CLASS_CODE_REGEX = /^[0-9]{5}/m;
const CLASS_DAY_HOUR_REGEX = /^(Su|Mo|Tu|We|Th|Fr|Sa)+\s[0-9]+:[0-9]+(AM|PM)\s-\s[0-9]+:[0-9]+(AM|PM)\n/mg;
const TIME_DAY_REGEX = /^(Su|Mo|Tu|We|Th|Fr|Sa)+/;
const TIME_HOUR_REGEX = /[0-9]+:[0-9]+(AM|PM)/g;
const DAY_STR_INT = { Su : 0, Mo : 1, Tu : 2, We : 3, Th : 4, Fr : 5, Sa : 6};

function parseCourses(unparsedString){
	var courses = [];
	resetRegex();
	var coursesStr = unparsedString.split(COURSE_DELIMITER);
	coursesStr.shift(); // gets rid of page header, navigation bar, and other junk
	for (var i = 0; i < coursesStr.length; i++) {
		var courseStr = coursesStr[i];
		var course = {};
		resetRegex();
		course.title = COURSE_TITLE_REGEX.exec(courseStr)[0];
		resetRegex();
		course.classes = parseClasses(course.title, courseStr);
		course.color = randomColor();
		course.isEnabled = true;
		courses.push(course);

	};
	return courses;
}

function parseClasses(course, unparsedString){
	
	var classes = new Array();

	var classStrings = unparsedString.split(CLASS_DELIMITER);
	classStrings.shift();
	

	for(var i=0;i<classStrings.length;i++){
		var classString = classStrings[i];
		var cl = parseClass(course, classString);
		if(cl){
			cl.isEnabled = true;
			classes.push(cl);

		}
	}

	return classes;


}

function parseClass(course, classString){
	resetRegex();
	var cl = {};
	cl.course = course;
	cl.classCode = CLASS_CODE_REGEX.exec(classString)[0];
	cl.times = new Array();
	var firstTimePos;
	var numTimesPerLine = new Array();
	while(CLASS_DAY_HOUR_REGEX.test(classString)){
		CLASS_DAY_HOUR_REGEX.lastIndex = 0;
		var timeStr = CLASS_DAY_HOUR_REGEX.exec(classString)[0];
		if(!firstTimePos){
			firstTimePos = classString.indexOf(timeStr);
		}
		classString = classString.replace(timeStr, "");
		var daysStr = TIME_DAY_REGEX.exec(timeStr)[0];
		for(var d=0;d<daysStr.length;d+=2){
			var classTime = {};
			classTime.isEnabled = true;
			classTime.day = DAY_STR_INT[daysStr.substr(d,2)];
			classTime.hours = new Array(2);
			TIME_HOUR_REGEX.lastIndex = 0;
			classTime.hours[0] = parseTime(TIME_HOUR_REGEX.exec(timeStr)[0]);
			classTime.hours[1] = parseTime(TIME_HOUR_REGEX.exec(timeStr)[0]);
			cl.times.push(classTime);
		}
		CLASS_DAY_HOUR_REGEX.lastIndex = 0;
		numTimesPerLine.push(daysStr.length/2);
	}

	for(var lt=0;lt<numTimesPerLine.length;lt++){
		var t=0;
		var room = classString.substring(firstTimePos, classString.indexOf("\n",firstTimePos)+1);
		classString = classString.replace(room, "");
		for(var i=0;i<numTimesPerLine[lt];i++){
			cl.times[t].room = room;
			t++
		}	
	}
	
	for(var lt=0;lt<numTimesPerLine.length;lt++){
		var t=0;
		var prof = classString.substring(firstTimePos, classString.indexOf("\n",firstTimePos)+1);
		classString = classString.replace(prof, "");
		for(var i=0;i<numTimesPerLine[lt];i++){
			cl.times[t].instructor = prof;
			t++;
		}	
		
	}

	/*cl.rooms = new Array();
	for(var r=0;r<cl.times.length;r++){
		var room = classString.substring(firstTimePos, classString.indexOf("\n",firstTimePos)+1);
		classString = classString.replace(room, "");
		cl.rooms.push(room);	
	}
	cl.profesors = new Array();
	for(var p=0;p<cl.times.length;p++){
		var prof = classString.substring(firstTimePos, classString.indexOf("\n",firstTimePos)+1);
		classString = classString.replace(prof, "");
		cl.profesors.push(prof);
	}*/

return cl;

/*	
{			course:,
			classCode:,
			times:[{
				day: , //as integer
				hours:[/*start,end],
				type: //Lecture, Lab, etc
			}]
		}*/
}

function parseTime(str){
	//X0:00AM
	var hour = parseInt(str.substring(0,str.indexOf(":")));
	var minutes = parseInt(str.substr(str.indexOf(":")+1,2));
	var apm = str.substr(str.indexOf("M")-1,2);

	return hour - (hour == 12?12:0) + (minutes/60) + (apm=="PM"?12:0);

}


var sampleClasses = [{

}]

function resetRegex(){
	COURSE_TITLE_REGEX.lastIndex = 0;
	CLASS_CODE_REGEX.lastIndex = 0;
	CLASS_DAY_HOUR_REGEX.lastIndex = 0;
	TIME_DAY_REGEX.lastIndex = 0;
	TIME_HOUR_REGEX.lastIndex = 0;
}
