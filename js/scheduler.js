

function getPossibleSchedules(allCourses, allOptions){
	var courses = new Array();
	for (var i = 0; i < allCourses.length; i++) {
		if(allCourses[i].isEnabled || allCourses[i] == null){
			courses.push(allCourses[i]);
		}
	};
	

	var numClasses = new Array(courses.length);
	var classIndex = new Array(courses.length);
	
	for(var i=0;i<courses.length;i++){
		numClasses[i] = courses[i].classes.length;
		classIndex[i] = 0;
	}
	

	var schedules = new Array();
	//loop runs through all combinations
	//of one class chosen from each course
	do{
		var schedule = new Array();
		for (var i = 0; i < courses.length; i++) {
			schedule.push(courses[i].classes[classIndex[i]]);
		};
		//console.log(schedule);
		var hasCollision = false;
		for(var c1=0;c1<schedule.length;c1++){
			var class1 = schedule[c1];
			if(!class1.isEnabled){hasCollision = true; continue;}
			for(var c2=c1+1;c2<schedule.length;c2++){
				var class2 = schedule[c2];
				if(!class2.isEnabled){hasCollision = true; continue;}
				for(var t1=0;t1<class1.times.length;t1++){
					var time1 = class1.times[t1];
					for(var t2=0;t2<class2.times.length;t2++){
						var time2 = class2.times[t2];
						if(time1.day == time2.day){
							if(time2.hours[0] >= time1.hours[0] 
								&& time2.hours[0]<=time1.hours[1]
								|| time1.hours[0]>= time2.hours[0]
								&& time1.hours[0]<= time2.hours[1]){
								hasCollision = true;
								/*console.log(class1.course + " conflicts with " + class2.course + 
									" on "+time1.day+"-"+time2.day + " at " + time1.hours + " and " + time2.hours);
								*/break;
							}
						}
					}
					if(hasCollision) break;
				}
				if(hasCollision) break;
			}
			if(hasCollision) break;
		}

		if(!hasCollision){
			schedules.push(schedule);
		}

	}while(increment(classIndex, numClasses, 1));

	return schedules;

}
//increments non-uniform base integer. Returns false if there is an overflow
function increment(digits, maxs, place){
	var index = digits.length-place;
	if(index < 0 || index >= digits.length){
		return false;
	}else{
		digits[index]++;
		if(digits[index] >= maxs[index]){
			digits[index] = 0;
			return increment(digits, maxs, place+1);
		}else{
			return true;
		}
	}
}

