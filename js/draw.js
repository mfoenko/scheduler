
const MAX_FONT_SIZE = 16;
const FONT_FAMILY = "Arial";
const DAY_OF_WEEK = ["Sun", "Mon", "Tue", "Wed", "Thu","Fri", "Sat"];
const TEXT_MARGIN = 2;

function drawClasses(canvas, classes){
	
	console.log(classes);

	var bounds = getScheduleBounds(classes);
	var numDays = bounds.latestDay-bounds.earliestDay+1+1;
	var numHours = Math.ceil(bounds.latestHour-bounds.earliestHour+1);
	var dayWidth = canvas.width/numDays;
	var hourHeight = canvas.height/numHours;

	var cx = canvas.getContext("2d");


	//draws grid
	cx.strokeStyle = "#BBBBBB";
	cx.lineWidth = 2;
	var path = new Path2D();
	for(var d=1;d<=numDays+1;d++){
		path.moveTo(d*dayWidth,0);
		path.lineTo(d*dayWidth,canvas.height)
	}
	for(var h=1;h<=numHours+1;h++){
		path.moveTo(dayWidth, h*hourHeight);
		path.lineTo(canvas.width, h*hourHeight);
	}
	cx.stroke(path);


	var fontSize = MAX_FONT_SIZE;
	do{
		cx.font = fontSize+"px "+FONT_FAMILY;
		fontSize--;
	}while(cx.measureText("XX:XXAM").width >= dayWidth && fontSize > 0);
	cx.textAlign = "center";
	cx.fillStyle = "0";
	

	//draws day headers
	for(var d = bounds.earliestDay;d<=bounds.latestDay;d++){
		var posX = (d-bounds.earliestDay+1)*dayWidth + .5*dayWidth;
		cx.fillText(DAY_OF_WEEK[d],posX, hourHeight-TEXT_MARGIN)
	}

	//draws hour headers
	cx.textAlign = "right";
	for(var h=bounds.earliestHour;h<=bounds.latestHour;h++){
		var posY = (h-bounds.earliestHour+1)*hourHeight+.5*hourHeight;
		cx.fillText(timeToString(h), dayWidth-TEXT_MARGIN, posY);
	}

	//numDays and Hours are offset by +1 to make space for headers
	cx.textAlign = "left";

	for(var c=0;c<classes.length;c++){
		fontSize = MAX_FONT_SIZE;
		for(var t=0;t<classes[c].times.length;t++){
			cx.fillStyle = courses[classes[c].course].color;
			var time = classes[c].times[t];
			var day = time.day - bounds.earliestDay +1;
			var startHour = time.hours[0] - bounds.earliestHour +1;
			var duration = time.hours[1] - time.hours[0];
			cx.fillRect(day*dayWidth, startHour*hourHeight, dayWidth, duration*hourHeight);
			var courseName = classes[c].course;
			courseName = courseName.substring(0,courseName.indexOf("-")-1);
			cx.fillStyle = "#000000";
			if(cx.measureText(courseName).width >= dayWidth){
				while(2*fontSize>duration*hourHeight){
					fontSize--;
				}
				cx.font = fontSize + "px "+FONT_FAMILY;
				cx.fillText(courseName.substring(0,courseName.indexOf(" ")), day*dayWidth, startHour*hourHeight+fontSize);
				cx.fillText(courseName.substr(courseName.indexOf(" ")), day*dayWidth, startHour*hourHeight+2*fontSize);
				//cx.fillText(time.instructor, day*dayWidth, startHour*hourHeight+3*fontSize);

			}else{
				cx.fillText(courseName, day*dayWidth, startHour*hourHeight+fontSize);
			}
		}
	}

	
}


function getScheduleBounds(schedule){
	

	var bounds = {};
	var firstTime = schedule[0].times[0];
	bounds.earliestHour = Math.floor(firstTime.hours[0]);
	bounds.latestHour = Math.ceil(firstTime.hours[1]);
	bounds.earliestDay = bounds.latestDay = firstTime.day;


	for (var c = 0; c < schedule.length; c++) {
		var cl = schedule[c];
		for (var t = 0; t < cl.times.length; t++) {
			var time = cl.times[t];
			if(time.hours[0] < bounds.earliestHour){
				bounds.earliestHour = Math.floor(time.hours[0]);
			}
			if(time.hours[1] > bounds.latestHour){
				bounds.latestHour = Math.ceil(time.hours[1]);
			}
			if(time.day < bounds.earliestDay){
				bounds.earliestDay = time.day;
			}
			if(time.day > bounds.latestDay){
				bounds.latestDay = time.day;
			}
		}
	}

	return bounds;
}

function timeToString(time){
	var hour = Math.floor(time);
	var dispHour = hour + 12*(hour == 0 || hour == 12) -12*(hour>=12);
	var dispMinute = Math.round((time%1)*60).toString();
	if(dispMinute.length == 1) dispMinute = "0"+dispMinute;
	var timeStr = dispHour.toString();
	timeStr += ":" + dispMinute +(time >= 11?"PM":"AM");
	return timeStr

}